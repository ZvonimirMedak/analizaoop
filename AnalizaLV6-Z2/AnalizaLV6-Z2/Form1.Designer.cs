﻿namespace AnalizaLV6_Z2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bStart = new System.Windows.Forms.Button();
            this.lbChancesLeft = new System.Windows.Forms.Label();
            this.lbWord = new System.Windows.Forms.Label();
            this.lbCor = new System.Windows.Forms.Label();
            this.lbInc = new System.Windows.Forms.Label();
            this.lbCorrectLetters = new System.Windows.Forms.Label();
            this.lbIncorrectLetters = new System.Windows.Forms.Label();
            this.lbSubButton = new System.Windows.Forms.Label();
            this.lbChars = new System.Windows.Forms.Label();
            this.tbGuess = new System.Windows.Forms.TextBox();
            this.bSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(12, 12);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(75, 23);
            this.bStart.TabIndex = 0;
            this.bStart.Text = "Start";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.bStart_Click_1);
            // 
            // lbChancesLeft
            // 
            this.lbChancesLeft.AutoSize = true;
            this.lbChancesLeft.Location = new System.Drawing.Point(182, 59);
            this.lbChancesLeft.Name = "lbChancesLeft";
            this.lbChancesLeft.Size = new System.Drawing.Size(91, 17);
            this.lbChancesLeft.TabIndex = 1;
            this.lbChancesLeft.Text = "Chances Left";
            // 
            // lbWord
            // 
            this.lbWord.AutoSize = true;
            this.lbWord.Location = new System.Drawing.Point(212, 106);
            this.lbWord.Name = "lbWord";
            this.lbWord.Size = new System.Drawing.Size(16, 17);
            this.lbWord.TabIndex = 2;
            this.lbWord.Text = "0";
            // 
            // lbCor
            // 
            this.lbCor.AutoSize = true;
            this.lbCor.Location = new System.Drawing.Point(76, 162);
            this.lbCor.Name = "lbCor";
            this.lbCor.Size = new System.Drawing.Size(102, 17);
            this.lbCor.TabIndex = 3;
            this.lbCor.Text = "Correct Letters";
            // 
            // lbInc
            // 
            this.lbInc.AutoSize = true;
            this.lbInc.Location = new System.Drawing.Point(323, 162);
            this.lbInc.Name = "lbInc";
            this.lbInc.Size = new System.Drawing.Size(111, 17);
            this.lbInc.TabIndex = 4;
            this.lbInc.Text = "Incorrect Letters";
            // 
            // lbCorrectLetters
            // 
            this.lbCorrectLetters.AutoSize = true;
            this.lbCorrectLetters.Location = new System.Drawing.Point(76, 193);
            this.lbCorrectLetters.Name = "lbCorrectLetters";
            this.lbCorrectLetters.Size = new System.Drawing.Size(0, 17);
            this.lbCorrectLetters.TabIndex = 5;
            // 
            // lbIncorrectLetters
            // 
            this.lbIncorrectLetters.AutoSize = true;
            this.lbIncorrectLetters.Location = new System.Drawing.Point(323, 193);
            this.lbIncorrectLetters.Name = "lbIncorrectLetters";
            this.lbIncorrectLetters.Size = new System.Drawing.Size(0, 17);
            this.lbIncorrectLetters.TabIndex = 6;
            // 
            // lbSubButton
            // 
            this.lbSubButton.AutoSize = true;
            this.lbSubButton.Location = new System.Drawing.Point(132, 350);
            this.lbSubButton.Name = "lbSubButton";
            this.lbSubButton.Size = new System.Drawing.Size(123, 17);
            this.lbSubButton.TabIndex = 7;
            this.lbSubButton.Text = "Whole word/Letter";
            // 
            // lbChars
            // 
            this.lbChars.AutoSize = true;
            this.lbChars.Location = new System.Drawing.Point(503, 106);
            this.lbChars.Name = "lbChars";
            this.lbChars.Size = new System.Drawing.Size(0, 17);
            this.lbChars.TabIndex = 8;
            // 
            // tbGuess
            // 
            this.tbGuess.Location = new System.Drawing.Point(290, 345);
            this.tbGuess.Name = "tbGuess";
            this.tbGuess.Size = new System.Drawing.Size(100, 22);
            this.tbGuess.TabIndex = 9;
            // 
            // bSubmit
            // 
            this.bSubmit.Location = new System.Drawing.Point(454, 344);
            this.bSubmit.Name = "bSubmit";
            this.bSubmit.Size = new System.Drawing.Size(75, 23);
            this.bSubmit.TabIndex = 10;
            this.bSubmit.Text = "Submit";
            this.bSubmit.UseVisualStyleBackColor = true;
            this.bSubmit.Click += new System.EventHandler(this.bSubmit_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bSubmit);
            this.Controls.Add(this.tbGuess);
            this.Controls.Add(this.lbChars);
            this.Controls.Add(this.lbSubButton);
            this.Controls.Add(this.lbIncorrectLetters);
            this.Controls.Add(this.lbCorrectLetters);
            this.Controls.Add(this.lbInc);
            this.Controls.Add(this.lbCor);
            this.Controls.Add(this.lbWord);
            this.Controls.Add(this.lbChancesLeft);
            this.Controls.Add(this.bStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.Label lbChancesLeft;
        private System.Windows.Forms.Label lbWord;
        private System.Windows.Forms.Label lbCor;
        private System.Windows.Forms.Label lbInc;
        private System.Windows.Forms.Label lbCorrectLetters;
        private System.Windows.Forms.Label lbIncorrectLetters;
        private System.Windows.Forms.Label lbSubButton;
        private System.Windows.Forms.Label lbChars;
        private System.Windows.Forms.TextBox tbGuess;
        private System.Windows.Forms.Button bSubmit;
    }
}

