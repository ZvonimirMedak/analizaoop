﻿namespace LV7Zad
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPotez = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.b11 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b13 = new System.Windows.Forms.Button();
            this.b23 = new System.Windows.Forms.Button();
            this.b22 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.b31 = new System.Windows.Forms.Button();
            this.b32 = new System.Windows.Forms.Button();
            this.b33 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.bSub = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbNer = new System.Windows.Forms.Label();
            this.lbPrvi = new System.Windows.Forms.Label();
            this.lbDrugi = new System.Windows.Forms.Label();
            this.bReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbPotez
            // 
            this.lbPotez.AutoSize = true;
            this.lbPotez.Location = new System.Drawing.Point(136, 48);
            this.lbPotez.Name = "lbPotez";
            this.lbPotez.Size = new System.Drawing.Size(0, 17);
            this.lbPotez.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(444, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "1. Igrac";
            // 
            // b11
            // 
            this.b11.Location = new System.Drawing.Point(12, 94);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(75, 71);
            this.b11.TabIndex = 2;
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Click += new System.EventHandler(this.b11_Click);
            // 
            // b12
            // 
            this.b12.Location = new System.Drawing.Point(138, 94);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(75, 70);
            this.b12.TabIndex = 3;
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Click += new System.EventHandler(this.b12_Click);
            // 
            // b13
            // 
            this.b13.Location = new System.Drawing.Point(271, 94);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(75, 70);
            this.b13.TabIndex = 4;
            this.b13.UseVisualStyleBackColor = true;
            this.b13.Click += new System.EventHandler(this.b13_Click);
            // 
            // b23
            // 
            this.b23.Location = new System.Drawing.Point(270, 194);
            this.b23.Name = "b23";
            this.b23.Size = new System.Drawing.Size(75, 72);
            this.b23.TabIndex = 5;
            this.b23.UseVisualStyleBackColor = true;
            this.b23.Click += new System.EventHandler(this.b23_Click);
            // 
            // b22
            // 
            this.b22.Location = new System.Drawing.Point(138, 194);
            this.b22.Name = "b22";
            this.b22.Size = new System.Drawing.Size(75, 71);
            this.b22.TabIndex = 6;
            this.b22.UseVisualStyleBackColor = true;
            this.b22.Click += new System.EventHandler(this.b22_Click);
            // 
            // b21
            // 
            this.b21.Location = new System.Drawing.Point(13, 194);
            this.b21.Name = "b21";
            this.b21.Size = new System.Drawing.Size(75, 71);
            this.b21.TabIndex = 7;
            this.b21.UseVisualStyleBackColor = true;
            this.b21.Click += new System.EventHandler(this.b21_Click);
            // 
            // b31
            // 
            this.b31.Location = new System.Drawing.Point(13, 302);
            this.b31.Name = "b31";
            this.b31.Size = new System.Drawing.Size(75, 70);
            this.b31.TabIndex = 8;
            this.b31.UseVisualStyleBackColor = true;
            this.b31.Click += new System.EventHandler(this.b31_Click);
            // 
            // b32
            // 
            this.b32.Location = new System.Drawing.Point(138, 303);
            this.b32.Name = "b32";
            this.b32.Size = new System.Drawing.Size(75, 69);
            this.b32.TabIndex = 9;
            this.b32.UseVisualStyleBackColor = true;
            this.b32.Click += new System.EventHandler(this.b32_Click);
            // 
            // b33
            // 
            this.b33.Location = new System.Drawing.Point(271, 302);
            this.b33.Name = "b33";
            this.b33.Size = new System.Drawing.Size(75, 68);
            this.b33.TabIndex = 10;
            this.b33.UseVisualStyleBackColor = true;
            this.b33.Click += new System.EventHandler(this.b33_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(447, 94);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 11;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(447, 209);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(444, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "2. igrac";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(13, 13);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 20;
            this.button10.Text = "Start";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // bSub
            // 
            this.bSub.Location = new System.Drawing.Point(461, 264);
            this.bSub.Name = "bSub";
            this.bSub.Size = new System.Drawing.Size(75, 23);
            this.bSub.TabIndex = 21;
            this.bSub.Text = "Submit";
            this.bSub.UseVisualStyleBackColor = true;
            this.bSub.Click += new System.EventHandler(this.bSub_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 314);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Prvi igrac ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(607, 314);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 23;
            this.label4.Text = "Drugi igrac";
            // 
            // lbNer
            // 
            this.lbNer.AutoSize = true;
            this.lbNer.Location = new System.Drawing.Point(520, 355);
            this.lbNer.Name = "lbNer";
            this.lbNer.Size = new System.Drawing.Size(16, 17);
            this.lbNer.TabIndex = 24;
            this.lbNer.Text = "0";
            // 
            // lbPrvi
            // 
            this.lbPrvi.AutoSize = true;
            this.lbPrvi.Location = new System.Drawing.Point(419, 353);
            this.lbPrvi.Name = "lbPrvi";
            this.lbPrvi.Size = new System.Drawing.Size(16, 17);
            this.lbPrvi.TabIndex = 25;
            this.lbPrvi.Text = "0";
            // 
            // lbDrugi
            // 
            this.lbDrugi.AutoSize = true;
            this.lbDrugi.Location = new System.Drawing.Point(607, 353);
            this.lbDrugi.Name = "lbDrugi";
            this.lbDrugi.Size = new System.Drawing.Size(16, 17);
            this.lbDrugi.TabIndex = 26;
            this.lbDrugi.Text = "0";
            // 
            // bReset
            // 
            this.bReset.Location = new System.Drawing.Point(496, 385);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(75, 53);
            this.bReset.TabIndex = 27;
            this.bReset.Text = "Reset Scores";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 450);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.lbDrugi);
            this.Controls.Add(this.lbPrvi);
            this.Controls.Add(this.lbNer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bSub);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.b33);
            this.Controls.Add(this.b32);
            this.Controls.Add(this.b31);
            this.Controls.Add(this.b21);
            this.Controls.Add(this.b22);
            this.Controls.Add(this.b23);
            this.Controls.Add(this.b13);
            this.Controls.Add(this.b12);
            this.Controls.Add(this.b11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbPotez);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbPotez;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b13;
        private System.Windows.Forms.Button b23;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Button b31;
        private System.Windows.Forms.Button b32;
        private System.Windows.Forms.Button b33;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button bSub;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbNer;
        private System.Windows.Forms.Label lbPrvi;
        private System.Windows.Forms.Label lbDrugi;
        private System.Windows.Forms.Button bReset;
    }
}

