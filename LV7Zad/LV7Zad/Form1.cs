﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV7Zad
{
    public partial class Form1 : Form
    {
        int red = 1;
        static string igracPrvi="", igracDrugi="";
        int potez = 0;

        public Form1()
        {
            InitializeComponent();
            disable();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (igracDrugi == "" || igracPrvi == "")
            {
                MessageBox.Show("Unesite imena igraca");
            }
            else
            {
                potez = 0;
                MessageBox.Show("Sretno!");
                clear();
                enable();
                lbPotez.Text = igracPrvi + " je na potezu";
            }
        }

        private void bSub_Click(object sender, EventArgs e)
        {
            try
            {
                igracPrvi = textBox1.Text;
                igracDrugi = textBox2.Text;
            }
            catch (Exception) { }
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            
        }

       
        private void pobjeda()
        {
            bool pobjeda = false;
            if (potez >= 5)
            {
                if ((b11.Text == b12.Text) && (b12.Text == b13.Text) && (b11.Text != "") && (!b11.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b21.Text == b22.Text) && (b22.Text == b23.Text) && (b23.Text != "") && (!b23.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b31.Text == b32.Text) && (b32.Text == b33.Text) && (b33.Text != "") && (!b33.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b11.Text == b21.Text) && (b21.Text == b31.Text) && (b11.Text != "") && (!b21.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b12.Text == b22.Text) && (b22.Text == b32.Text) && (b12.Text != "") && (!b12.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b13.Text == b23.Text) && (b23.Text == b33.Text) && (b33.Text != "") && (!b33.Enabled))
                {
                    pobjeda = true;
                }

                else if ((b11.Text == b22.Text) && (b22.Text == b33.Text) && ( b11.Text !="")&& (!b33.Enabled))
                {
                    pobjeda = true;
                }
                else if ((b13.Text == b22.Text) && (b22.Text == b31.Text) && (b22.Text != "") && (!b22.Enabled))
                {
                    pobjeda = true;

                }


                if (pobjeda)
                {
                    disable();
                    String pobjednik = "";
                    if (red == 1)
                    {
                        pobjednik = igracDrugi;
                        lbDrugi.Text = (Int32.Parse(lbDrugi.Text) + 1).ToString();



                    }
                    else
                    {
                        pobjednik = igracPrvi;
                        lbPrvi.Text = (Int32.Parse(lbPrvi.Text) + 1).ToString();
                        
                    }
                    MessageBox.Show(pobjednik + " je pobjednik!");
                   
                }
                else
                {
                    if (potez == 9)
                    {
                        lbNer.Text = (Int32.Parse(lbNer.Text) + 1).ToString();
                        MessageBox.Show("Nerijeseno!");
                        
                    }
                }
            }
        }
        private void disable()
        {
            b11.Enabled = false;
            b12.Enabled = false;
            b13.Enabled = false;
            b21.Enabled = false;
            b22.Enabled = false;
            b23.Enabled = false;
            b31.Enabled = false;
            b32.Enabled = false;
            b33.Enabled = false;

        }
        private void clear()
        {
            b11.Text = "";
            b12.Text = "";
            b13.Text = "";
            b21.Text = "";
            b22.Text = "";
            b23.Text = "";
            b31.Text = "";
            b32.Text = "";
            b33.Text = "";
        }
 


        private void b11_Click(object sender, EventArgs e)
        {
            if(red == 1)
            {
                b11.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b11.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b11.Enabled = false;
            pobjeda();

        }

        private void b12_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b12.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b12.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b12.Enabled = false;
            pobjeda();
        }

        private void b13_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b13.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b13.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b13.Enabled = false;
            pobjeda();
        }

        private void b21_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b21.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b21.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b21.Enabled = false;
            pobjeda();
        }

        private void b22_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b22.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b22.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b22.Enabled = false;
            pobjeda();
        }

        private void b23_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b23.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b23.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b23.Enabled = false;
            pobjeda();
        }

        private void b31_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b31.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b31.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b31.Enabled = false;
            pobjeda();
        }

        private void b32_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b32.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b32.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b32.Enabled = false;
            pobjeda();
        }

        private void b33_Click(object sender, EventArgs e)
        {
            if (red == 1)
            {
                b33.Text = "X";
                red = 0;
                lbPotez.Text = igracDrugi + " je na potezu";
            }
            else
            {
                b33.Text = "O";
                red = 1;
                lbPotez.Text = igracPrvi + " je na potezu";
            }
            potez++;
            b33.Enabled = false;
            pobjeda();
        }

        private void bReset_Click(object sender, EventArgs e)
        {

            lbDrugi.Text = "0";
            lbPrvi.Text = "0";
            lbNer.Text = "0";
        }

        private void enable()
        {
            b11.Enabled = true;
            b12.Enabled = true;
            b13.Enabled = true;
            b21.Enabled = true;
            b22.Enabled = true;
            b23.Enabled = true;
            b31.Enabled = true;
            b32.Enabled = true;
            b33.Enabled = true;

        }
    }
}
